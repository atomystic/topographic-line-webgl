// Variables
uniform float uTime;
varying vec2 vUv;
varying float vElevation;
uniform float uFrequencyStroke;
uniform float uStrokeWidth;

float getStrokeWidth(float elevation, float fac){

   float  el =  (sin(elevation*uFrequencyStroke*fac)+1.0)/2.0;
   float strokeWidth;

   strokeWidth= step(el*fac,uStrokeWidth);
   strokeWidth = clamp(strokeWidth,0.0,1.0);

    return strokeWidth;

}

void main()
{


    float elevation  =  vElevation;
    
    float fac1 = 1.0;
    float fac2 = 3.0;

    // stroke width
    float strokeWidth = getStrokeWidth(elevation,fac1);

    float strokeWidth2 = getStrokeWidth(elevation,fac2);

    // float finalStroke =strokeWidth+strokeWidth2;
    float finalStroke =strokeWidth;


    //move factor
    // vec2 vUvMove = (sin(vec2(vUv.x+uTime,vUv.y))+1.0)/2.0; 


    // second color
    vec2 uvColor = vec2(finalStroke);

    // color
    // vec4 color = vec4(uvColor,finalStroke,1.0);same 1
    vec4 color = vec4(vec3(finalStroke),1.0);
    

     gl_FragColor = color ;

}