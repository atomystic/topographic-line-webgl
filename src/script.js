import './style.css'
import * as THREE from 'three'
import { OrbitControls } from 'three/examples/jsm/controls/OrbitControls.js'
import * as dat from 'dat.gui'
import testVertexShader from './shaders/test/vertex.glsl'
import testFragmentShader from './shaders/test/fragment.glsl'



// Debug
const gui = new dat.GUI()

// Stats

// Canvas
const canvas = document.querySelector('canvas.webgl')

// Size
const sizes = {
    width: window.innerWidth,
    height: window.innerHeight
}

// Scene
const scene = new THREE.Scene()

// Mesh
const planeGeometry = new THREE.PlaneGeometry(300, 300, 500, 500);
const planeMaterial = new THREE.ShaderMaterial({

    vertexShader: testVertexShader,
    fragmentShader: testFragmentShader,
    side: THREE.DoubleSide,
    transparent: true,

    uniforms: {

        uTime: { value: 0 },
        uHeightFactor: { value: 10 },
        uFrequencyX: { value: 14 },
        uFrequencyY: { value: 9 },
        uFrequencyStroke: { value: 3 },
        uStrokeWidth: { value: 0.006 }

    }
});

gui.add(planeMaterial.uniforms.uHeightFactor, "value").min(1).max(20).name("Height factor")

gui.add(planeMaterial.uniforms.uFrequencyX, "value").min(1).max(20).name("Frequency X")
gui.add(planeMaterial.uniforms.uFrequencyY, "value").min(1).max(20).name("Frequency Y")

gui.add(planeMaterial.uniforms.uFrequencyStroke, "value").min(0.1).max(7).name("Frequency stroke")
gui.add(planeMaterial.uniforms.uStrokeWidth, "value").min(0.001).max(0.1).name("Stroke width")

const plane = new THREE.Mesh(planeGeometry, planeMaterial);

plane.rotation.x = Math.PI / 2

scene.add(plane)




/**
 * Camera
 */
let camera = new THREE.PerspectiveCamera()

// Base camera
camera = new THREE.PerspectiveCamera(75, sizes.width / sizes.height, 0.01, 1000)
camera.position.set(0, 25, -70)
scene.add(camera)

window.addEventListener('resize', () => {
    // Update sizes
    sizes.width = window.innerWidth
    sizes.height = window.innerHeight

    // Update camera
    camera.aspect = sizes.width / sizes.height
    camera.updateProjectionMatrix();

    // Update renderer
    renderer.setSize(sizes.width, sizes.height)
    renderer.setPixelRatio(Math.min(window.devicePixelRatio, 2))
})



// Controls
const controls = new OrbitControls(camera, canvas)
controls.enableDamping = true
controls.enabled = true

/**
 * Renderer
 */
const renderer = new THREE.WebGLRenderer({
    canvas: canvas
})

renderer.setSize(sizes.width, sizes.height)
renderer.setPixelRatio(Math.min(window.devicePixelRatio, 2))

let clock = new THREE.Clock();
const tick = () => {

    // console.log(clock.getElapsedTime())
    // Mis à jour de uTime

    planeMaterial.uniforms.uTime.value = clock.getElapsedTime();

    // Update controls
    controls.update()

    renderer.render(scene, camera)

    window.requestAnimationFrame(tick)

}

tick()